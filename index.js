// imports
const http = require("http");

// variables
const PORT = 3000;
const HOST = "localhost";

// server
const server = http.createServer((req, res) => {
    switch (req.url) {
        case "/login":
            res.writeHead(200, {"Content-Type" : "text/plain"})
            res.end("Welcome to login page");
            break;
    
        default:
            res.writeHead(404, {"Content-Type" : "text/plain"})
            res.end("I'm sorry the page you are looking for cannot be found");
            break;
    }
});
server.listen(PORT);

console.log(`Server is now accessible at ${HOST}:${PORT}`);